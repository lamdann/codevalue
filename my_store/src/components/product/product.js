/**
 * Created by Lamdan on 3/28/2018.
 */
import React,{Component} from 'react';
import './product.css'
import { connect } from 'react-redux';
import * as actionCreators from '../../store/actions/index';

class product extends Component{

    constructor(props){
        super(props);
        this.state = {...props};
    }

    componentWillUpdate(nextProps){
    }
    markSearch=(str)=>{
     let rez =[];
     let strTmp;
        if(!this.props.prod.prdt.search){
            return <h3>{str}</h3>
        }else{
         let s = this.props.prod.prdt.search.toLowerCase();
         let index= str.toLowerCase().indexOf(s);
         if(index>-1){
         let char = str.slice(index,index+s.length);
          while( index >-1){
              if(index===0){
                  char = str.slice(index,index+s.length);
                  rez.push(<span style={{background: "red"}} key ={rez.length}>{char}</span>);
                  str =str.substring(index+char.length,str.length);
              }else{
                  strTmp = str.substring(0,index);
                  rez.push(<span key ={rez.length}>{strTmp}</span>);
                  char = str.slice(index,index+s.length);

                  rez.push(<span style={{background: "red"}}  key ={rez.length}>{char}</span>);
                  str =str.substring(index+char.length,str.length);
              }
              index = str.toLowerCase().indexOf(char.toLowerCase());
              if(index < 0 ){ rez.push(<span key ={rez.length} >{str}</span>); }
          }
        return  <h3>{rez}</h3>;
         }
        return <h3>{str}</h3>;
        }
    };

    render(){
        return(
            <div className="Product" onClick={()=>this.props.onEdit(this.props.id)}>
                <div className="ProductLeftGroup">
                    <div >
                        <img src ={this.props.imgSrc} alt ='Product' className="ProductImage"/>
                    </div>
                    <div className="ProductDescriptionGroup">
                       {/* If User do Searching - highlight the searching string;*/}
                       {this.markSearch(this.props.name)}
                       <textarea disabled
                       placeholder={this.props.desc}
                       />
                    </div>
                </div>

                     <button onClick={()=>this.props.onDelete(this.props.id)}
                             className="btn btn-danger">Delete</button>

            </div>
        );
    }
}
const mapStateToProps = state => {
    return {
        prod: state
    }
};

const mapDispatchToProps = dispatch => {
    return {
        onDelete:(value) => dispatch(actionCreators.delete_prod(value)),
        onEdit: (value) => dispatch(actionCreators.edit(value))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)( product)