/**
 * Created by Lamdan on 3/28/2018.
 */
import React, {Component} from "react";
import  './productFooter.css';
import * as actionCreators from '../../store/actions/index';
import { connect } from 'react-redux';

class productFooter extends Component{

 constructor(props){
     super(props);

     this.currentPage = 1;
     this.isNexPage    ='enabled';
     this.isPrevPage = "disabled";
     this.numPages = props.numPages;
 }


      componentWillUpdate(nextProps){
      this.currentPage =nextProps.props.wcnt.currentPage;
      if(!this.currentPage)this.currentPage =1;
      this.isNexPage  =  nextProps.props.wcnt.currentPage>=this.numPages?"disabled":'enabled';
      this.isPrevPage =  nextProps.props.wcnt.currentPage>1?'enabled':"disabled";
      console.log("FOOTER_UPDATE_PROPS_",nextProps );
    }

 render(){

     return(
         <div className="footerContainer">
             <div className="footerTools">
                 <div>
                     <i className={"fa fa-arrow-left " +this.isPrevPage}
                 onClick={()=>this.props.onBefore(--this.currentPage)}/>
                 </div>
                 <div>
                 <span style = {{fontSize:'20px'}}> {this.currentPage} </span>
                 </div>
                 <div>
                     <i className= {"fa fa-arrow-right " +this.isNexPage}
                  onClick={()=>this.props.onNext(++this.currentPage)}/>
                 </div>
             </div>
        </div>
     );
 }

}

const mapStateToProps = state => {
    return {
        props: state
    }
};
const mapDispatchToProps = dispatch => {
    return {
        onNext:(e)=>dispatch(actionCreators.next(e)),
        onBefore:(e)=>dispatch(actionCreators.before(e))
    }
};


export default connect(mapStateToProps, mapDispatchToProps)(productFooter);