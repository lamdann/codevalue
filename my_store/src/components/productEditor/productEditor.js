/**
 * Created by Lamdan on 3/28/2018.
 */
import React, {PureComponent} from 'react'
import "./productEditor.css"
import { connect } from 'react-redux';
import * as actionCreators from '../../store/actions/index';


class productEditor extends PureComponent{
    constructor(props){
        super(props);

        this.product ={...props.props};
        if(!this.product.id)
            this.product ={...props};
        this.state={
           myForm:{
               name:false,
               url:false,
               desc:false,
               price:false
           }
        };
    }
    validation = (name,e)=>{
        let myform = {...this.state.myForm};
            myform[name] =e.target.value.length>0;
            if(myform[name]!==this.state.myForm[name]){
            this.setState({'myForm': myform});

            }
    };
    componentWillUpdate(nextProps,nexState){
        this.product ={...nextProps};

    }
    componentWillReceiveProps(nextProps){

        this.product ={...nextProps.props};

    }

    componentDidMount(){
        let  myForm ={
            name:false,
            url:false,
            desc:false,
            price:false
        };
        this.setState({'myForm': myForm});
    }
    componentDidUpdate(prevProps, prevState){

    }


    textEriaHandler =(e)=>{
        switch (e.type){
            case "blur":
                if(e.target.value.length>0)
                { e.target.value.length>200?  this.product.desc = e.target.value.slice(0,200):
                  this.product.desc =e.target.value;  console.log("onBlur",this.product);
                }
                console.log("Blur event");

            break;
            case "focus":
                if(!e.target.value){
                e.target.value =this.product.desc;
                this.validation("desc",e)
                }
                console.log("focus event");

            break;
            case "input":
                this.validation("desc",e);
                console.log("Input ivent");
            break;
            default: return;
        }
    }

    render(){
        return(
            <div className="ProductEditorContainer form-group-lg">
                    <img src={this.product.imgSrc} alt=""/>

                <div className="form-group form-control-lg  has-success has-feedback">
                    <span className="glyphicon glyphicon-ok form-control-feedback" hidden ={!this.state.myForm.url}/>
                    <label htmlFor="imgUrl">URL</label>
                   <input type="text" id="imgUrl"
                          placeholder={this.product.imgSrc}
                          onFocus={(e)=>{e.target.value = this.product.imgSrc;this.validation("url",e) }}
                          onBlur={(e)=>{this.product.imgSrc = e.target.value }}
                          className="form-control-lg"  onInput={(e)=>this.validation("url",e)}/>

                 </div>


                <div className="form-group form-control-lg  has-success has-feedback">
                  <input type="text" id = "ProductName"
                         placeholder={this.product.name}
                         onFocus={(e)=>{e.target.value = this.product.name;this.validation("name",e) }}
                         onBlur={(e)=>{this.product.name=e.target.value; }}
                         onInput={(e)=>this.validation("name",e)}
                         className="form-control" />
                    <span className="glyphicon glyphicon-ok form-control-feedback" hidden ={!this.state.myForm.name}/>
                </div>

                <div className="form-group form-control-lg  has-success has-feedback">
                    <textarea placeholder={this.product.desc}
                              onFocus={this.textEriaHandler}
                              onBlur={this.textEriaHandler}
                              onInput={this.textEriaHandler}
                              className="form-control EditProductDescription"/>
                    <span className="glyphicon glyphicon-ok form-control-feedback" hidden ={!this.state.myForm.desc}/>
                </div>
                <div className="form-group form-control-lg  has-success has-feedback">
                    <span className="glyphicon glyphicon-ok form-control-feedback" hidden ={!this.state.myForm.price}/>
                    <label htmlFor="price">Price</label>
                    <input type="number" placeholder={this.product.price}
                           onFocus={(e)=>{e.target.value = this.product.price; this.validation("price",e)}}
                           onBlur={(e)=>{this.product.price = e.target.value; }}
                           onInput={(e)=>{this.validation("price",e);this.product.price = e.target.value;}}
                           min ={0} />

                </div>

                <div className="rightAlign">
                        <button className="btn btn-success ProductEditorSaveBtn"
                                disabled={!(this.state.myForm.desc && this.state.myForm.name
                                && this.state.myForm.price && this.state.myForm.url)}
                                onClick={(e)=>{
                                    this.product.editedDate =new Date();
                                    this.props.onSave(this.product);
                                    console.log("SavedProduct",this.product);
                                }}>Save</button>

                </div>
            </div>
        );
    }
}
const mapStateToProps = state => {
    return {
        prodEdit: {...state}
    }
};

const mapDispatchToProps = dispatch => {
    return {
        onSave:(value) => dispatch(actionCreators.save_product(value))
    }
};
export default connect(mapStateToProps, mapDispatchToProps)(productEditor)