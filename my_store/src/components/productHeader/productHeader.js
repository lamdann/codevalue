/**
 * Created by Lamdan on 3/28/2018.
 */
import React,{ PureComponent } from 'react';
import './productHeader.css';
import { connect } from 'react-redux';
import * as actionCreators from '../../store/actions/index';


class productHeader extends PureComponent{


    render(){
        return(
            <div className="form-inline">
                <div className="form-group ProductHeader">
                    <button className="bnt btn-success btn-lg"
                    onClick={(e)=>{this.props.addNewProduct()}}
                    >+Add</button>

                    <div className="  input-group ProductHeaderSearch">

                        <input type="text" className="form-control searchInput" placeholder="Search product by name"
                        onInput={(e)=>{this.props.onSearch(e.target.value)}}

                        />
                            <div className="input-group-btn ">
                                <i className="glyphicon glyphicon-search" style={{fontSize:"24px"}}/>
                             </div>
                    </div>

                    <div className="input-group">
                        <label htmlFor ="sel1">Sort by :
                            <select className="form-control " id="sel1" onInput={(e)=>this.props.onSort(e.target.value)}>
                                <option>Sort by</option>
                                <option value="name">Name</option>
                                <option value="date">Date</option>
                            </select>
                        </label>
                    </div>
                </div>
            </div>
        )
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onSort:(value)=>dispatch(actionCreators.sort(value)),
        onSearch:(value)=>dispatch(actionCreators.search(value))
    }
};
export default connect(null, mapDispatchToProps)( productHeader);