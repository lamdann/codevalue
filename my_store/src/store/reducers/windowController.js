/**
 * Created by Lamdan on 3/28/2018.
 */
import * as actionTypes from '../actions/actionTypes';

import   {initialState} from './productsListHandler';

import { updateObject } from '../utility';
const reducer = ( state = initialState, action ) => {


    switch ( action.type ) {
        case actionTypes.NEXT_PAGE:

            return updateObject(state, {currentPage: action.val});
        case actionTypes.BEFORE_PAGE:
            return updateObject(state, {currentPage: action.val});
        default: return state;
    }

};

export default reducer;
