import * as actionTypes from '../actions/actionTypes';
import { updateObject } from '../utility';

let json= localStorage.getItem("List_1");
if(!json) {
    json = `[{"imgSrc":"https://cdn.pixabay.com/photo/2018/03/14/23/36/sunglasses-3226708__340.jpg",
           "name":"SunGlasses",
           "desc":"Very Good Product",
           "id":1,
           "price":0,
           "creationDate":"2018-03-20",
           "editedDate":""
           },
           {
           "imgSrc":"https://cdn.pixabay.com/photo/2018/02/27/14/07/white-wine-3185546__340.jpg",
           "name":"Vine",
           "desc":"Very Good Vine",
           "id":2,
           "price":0,
           "creationDate":"2018-03-21",
           "editedDate":""
           },
           {
           "imgSrc":"https://cdn.pixabay.com/photo/2018/01/28/21/14/lens-3114729_960_720.jpg",
           "name":"Zorkiy Photo Camera",
           "desc":"Very Good Camera",
           "id":3,
           "price":0,
           "creationDate":"2018-03-22",
           "editedDate":""
           }]`;
    localStorage.setItem('List_', json);
}

export const initialState ={productList:JSON.parse(json)};

const reducer = ( state = initialState, action ) => {
    let productListCopy =[];
    let newState;

    switch ( action.type ) {

        case actionTypes.INCREMENT:
        return updateObject(state, {counter: state.counter + 1});

        case actionTypes.DELETE:
            productListCopy=state.productList.filter( (el) => el.id!==action.val);
            newState = updateObject(state,{'productList':productListCopy});
            localStorage.setItem("List_1",JSON.stringify(productListCopy));
            return newState;

        case actionTypes.SAVE:
                productListCopy = arrayCopy(state.productList);
                console.log("ONSAVE ARRAY COPY",productListCopy);
                console.log("action.val",action.val);
                let index = productListCopy.findIndex((el) => el.id === action.val.id);
                if (index > -1) {
                    productListCopy[index] = updateObject(productListCopy[index], action.val);
                    newState = updateObject(state, {'productList': productListCopy});
                    localStorage.clear();
                    localStorage.setItem("List_1",JSON.stringify(productListCopy));
                  console.log("newState",newState);
                    return newState;
                }else{
                    productListCopy.push(action.val);
                    localStorage.clear();
                    localStorage.setItem("List_1",JSON.stringify(productListCopy));
                    console.log("NEW_LIST ",productListCopy);
                    return updateObject(state, {'productList': productListCopy});
                }


        case actionTypes.ADD:
            return updateObject(state, {'editNewProduct':true});

        case actionTypes.EDIT:
             index = state.productList.findIndex((el) => el.id === action.val);
             newState = updateObject(state,{'editNewProduct':false,'numProductToEdit':index} );
             return newState;

        case actionTypes.SORT:
           return updateObject(state,{'sortBy':action.val} );

        case actionTypes.SEARCH:
            let search =action.val?action.val:false;
            return updateObject(state,{'search':search} );
        default: return state;
    }

    function arrayCopy(array) {
        let copyArray;
        copyArray = array.map((el)=>{
            if( Array.isArray(el)){
               return arrayCopy(el);
            }
            else return updateObject({},el);
        });
        return copyArray;
    }

};
export default reducer;