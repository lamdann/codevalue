import * as actionTypes from './actionTypes';


export const increment = () => {
    return {
        type: actionTypes.INCREMENT
    };
};


export const delete_prod = (value) => {
       return{
        type: actionTypes.DELETE,
        val: value
       };
};
export const save_product = (value ) => {
    return {
        type: actionTypes.SAVE,
        val: value
    };
};
export const edit = (value ) => {
    return {
        type: actionTypes.EDIT,
        val: value
    };
};
export const add = ( value ) => {
    return {
        type: actionTypes.ADD,
        val: value
    };
};
export const sort = ( value ) => {
    return {
        type: actionTypes.SORT,
        val: value
    };
};
export const search = ( value ) => {
    return {
        type: actionTypes.SEARCH,
        val: value
    };
};

