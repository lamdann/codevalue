/**
 * Created by Lamdan on 3/28/2018.
 */
import * as actionTypes from './actionTypes';
export const next = (value ) => {
    return {
        type: actionTypes.NEXT_PAGE,
        val: value
    };
};
export const before = (value ) => {
    return {
        type: actionTypes.BEFORE_PAGE,
        val: value
    };
};


