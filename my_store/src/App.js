import React, { PureComponent } from 'react';
import './App.css';
import Product from '../src/components/product/product.js';
import { connect } from 'react-redux';
import ProductHeader from "../src/components/productHeader/productHeader";
import ProductEditor from '../src/components/productEditor/productEditor'
import * as actionCreators from './store/actions/index';
import Footer from "../src/components/productFooter/productFooter";

class App extends PureComponent{

    constructor(props){
       super(props);


       this.productToEdit = null;
       if(props.initialData.prdt.productList) {
           this.pL = [...this.props.initialData.prdt.productList];

           this.indexArray = this.pL.map((el) => {
               return el.id;
           });
       }
       this.sortListBy =null;
       this.editNewProduct =false;
       this.search = false;
       this.currentPage=1;
       this.pageSize=2;
       this.numPages = props.initialData.prdt.productList.length / this.pageSize;
       this.state ={editNewProduct:false,sortBy:"",search:false,currentPage:1,pageSize:this.pageSize,numPages:this.numPages};
    }

    componentWillReceiveProps(nextProps){

        this.editNewProduct = nextProps.props.prdt.editNewProduct;
    }
    componentWillUpdate(nextProps){

        this.pL = [...nextProps.props.prdt.productList];

        this.numPages = nextProps.props.prdt.productList.length/this.pageSize;
        this.numProductToEdit = nextProps.props.prdt.numProductToEdit;
        this.sortListBy =   nextProps.props.prdt.sortBy;
        this.search =nextProps.props.prdt.search;


        this.currentPage = (nextProps.props.wcnt.currentPage&&nextProps.props.wcnt.currentPage>0)?
            nextProps.props.wcnt.currentPage:1;
        this.currentPage =this.currentPage*this.pageSize <=this.pL.length +this.pageSize?this.currentPage:this.currentPage-1;
    }

    doSort(list){

      switch (this.sortListBy) {
          case 'name': return this.sortByName(list);
          case 'date': return this.sortByDate(list);
          default : return list;
      }
    }
    sortByName =(list)=>{
        list.sort((a,b)=>{
            return a.name>b.name;
        })
    };
    sortByDate=(list)=>{
        list.sort((a,b)=>{
            return    Date.parse(a.creationDate)<Date.parse(b.creationDate);
        })
    };

    getNewProductTemplate =()=>{
       // let index = this.getFreeIndex();
      //  let date = new Date().toDateString();
       let prod= JSON.parse( `{"imgSrc":"",
            "name":"New Product",
            "desc":"New Product",
            "id":${this.getFreeIndex()},
            "price":0,
            "creationDate":"${new Date()}",
            "editedDate":""
        }` );
       let np = <ProductEditor {...prod}/>;
       return(np);
    };
    addNewProduct=()=>{
       this.props.onAdd();
    };

    realiseIndex(old){
        let index = this.indexArray.indexOf(old);
        this.indexArray.splice(index,1);
    }
    getFreeIndex=()=>{
        let freeIndex=null;
        let nextIndex =true;
        while(nextIndex) {
            freeIndex = Math.random();
            if(this.indexArray.indexOf(freeIndex)===-1){
                nextIndex =false;
                this.indexArray.push(freeIndex);
            }
        }
        return freeIndex;
    };

    getPage =(numPage,list)=>{
        if(!list){return null}
        let end = numPage!==0?(this.pageSize*numPage)<=list.length?(this.pageSize*numPage):list.length:this.pageSize;
        let start = (end-this.pageSize)<0?0:end-this.pageSize;
        return list.slice(start,end);
    };

 render()
  {
             let arrProducts = null;
             this.productToEdit =null;
              if (this.pL){
              let listPart ;

             //Searching if User type on Search input
              if(this.search){
                  let filteredPL = this.pL.filter((el)=>{
                      return el.name.toLowerCase().indexOf(this.search.toLowerCase())>-1;
                  });
                listPart =this.getPage(this.currentPage,filteredPL);
              }else {
                 listPart = this.getPage(this.currentPage,this.pL);
              }
              //Sorting Part of the  Products List
             if (listPart && this.sortListBy) this.doSort(listPart);

              arrProducts = listPart.map((el) => {
                  return (<Product {...el} key = {el.id}/>);
              });  }
                //If we have product to Edit
              if(this.numProductToEdit!==undefined && this.numProductToEdit>-1){
                  /*
                  let prod = arrProducts.findIndex( (el) => el.props.id===this.numProductToEdit);
                  this.productToEdit = <ProductEditor {...arrProducts[prod]}/>;
                  */
                  let prod = this.pL[this.numProductToEdit];
                  this.productToEdit = <ProductEditor {...prod}/>;

              }
             this.productToEdit = this.editNewProduct?this.getNewProductTemplate():this.productToEdit;


            return (
              <div className="App">
                <header className="App-header">
                  <div className="App-logo">
                    <p>My Store</p>
                  </div>
                </header>
                  <div className="row">
                      <div className="col-sm-6">
                          <div className="ProductsContainer ">
                              <ProductHeader addNewProduct = {this.addNewProduct}/>
                              {arrProducts}
                          </div>
                      </div>
                      <div className="col-sm-6">
                          <div className="ProductsContainer">
                              {this.productToEdit}
                          </div>
                      </div>
                  </div>
                  <Footer numPages = {this.numPages} />
              </div>
            );
  }
}

 const mapDispatchToProps = dispatch => {
    return {
        onAdd:()=>dispatch(actionCreators.add())
    }};

const mapStateToProps = state => {
    return {
        props: state
    }
};
export default connect(mapStateToProps, mapDispatchToProps)(App);
