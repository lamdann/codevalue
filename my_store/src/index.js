import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import ProductReducer from './store/reducers/productsListHandler';
import WindowController from './store/reducers/windowController';
import { Provider } from 'react-redux';

import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
//import mapDispatchToProps from "react-redux/es/connect/mapDispatchToProps";


const rootReducer = combineReducers({
    prdt: ProductReducer,
    wcnt: WindowController
});

const logger = store => {
    return next => {
        return action => {
            console.log('[Middleware] Dispatching', action);
            const result = next(action);
            console.log('[Middleware] next state', store.getState());
            return result;
        }
    }
};
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(rootReducer,composeEnhancers(applyMiddleware(logger,thunk)));

ReactDOM.render(<Provider store={store}><App initialData={store.getState()}/></Provider>,document.getElementById('root') );
registerServiceWorker();

//	return applyMiddleware(thunk,consoleMessages)(createStore)(appReducer, initialState)
